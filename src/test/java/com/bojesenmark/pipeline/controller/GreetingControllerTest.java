package com.bojesenmark.pipeline.controller;

import com.bojesenmark.pipeline.model.Greeting;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
public class GreetingControllerTest {

    private MockMvc mockMvc;

    @Test
    public void testGreetingController() {
        Greeting greeting = new Greeting(1, "Default test value");
        Assertions.assertNotNull(greeting);
        Assertions.assertEquals(1, greeting.getId());
        Assertions.assertEquals("Default test value", greeting.getContent());
    }
}
